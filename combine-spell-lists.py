import json

# Because missing spells are already cleaner than srd spells.
def clean_missing_spell(spell_json):
    # Extract the spell lvl and type for changing.
    spell_json['contents'][0] = spell_json['contents'][0] \
                                    .replace('<i>', '') \
                                    .replace('</i>', '')

    return spell_json

def clean_spell(spell_json):
    # Extract the spell lvl and type for changing.
    lvl_type = spell_json['contents'][0].split(' ')

    # lvl_type: Change from using 'text' to 'subtitle'.
    lvl_type[0] = 'subtitle'

    # lvl_type: Remove the '<i>' tags.
    lvl_type[2] = lvl_type[2][3:]
    lvl_type[-1] = lvl_type[-1][:-4]

    # lvl_type: Remove '-' in '1st-level'.
    if 'level' in spell_json['contents'][0]:
        lvl_type[2] = lvl_type[2].replace('-', ' ')

    # lvl_type: Capitalise spell type.
    if 'level' in spell_json['contents'][0]:
        lvl_type[3] = lvl_type[3].title()
    else:
        lvl_type[2] = lvl_type[2].title()

    # Set new lvl_type.
    spell_json['contents'][0] = ' '.join(lvl_type)

    # Add commas between VSM characters.
    spell_json['contents'][4] = spell_json['contents'][4] \
        .replace('VSM', 'V, S, M') \
        .replace('VS', 'V, S') \
        .replace('VM', 'V, M') \
        .replace('SM', 'S, M')

    # Remove any empty strings at the end of contents.
    if spell_json['contents'][-1] == '':
        del(spell_json['contents'][-1])

    # Change all fills to 1.
    for i, line in enumerate(spell_json['contents']):
        if line.startswith('fill | ') and len(line) == 8:
            spell_json['contents'][i] = 'fill | 1'

    return spell_json

if __name__ == "__main__":
    combined_spells = []

    # Grab all the missing spells.
    with open('data/cleaned/missing-spells-trimmed.json', 'r') as f:
        spell_data = json.load(f)
        for spell in spell_data:
            combined_spells.append(clean_missing_spell(spell))

    # Grab all the srd spells.
    with open('data/raw/spell-cards-srd.json', 'r') as f:
        spell_data = json.load(f)
        for spell in spell_data:
            combined_spells.append(clean_spell(spell))

    # Sort spells by PHB page.
    combined_spells = sorted(combined_spells, key=lambda x: x['contents'][-1] \
                                                            .split(' ')[-1] \
                                                            .split('-')[0])

    # Save all spells together.
    with open('data/cleaned/all-spells.json', 'w') as f:
        f.write(json.dumps(combined_spells, indent=2, sort_keys=True))
