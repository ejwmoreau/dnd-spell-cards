import sys
import json
import copy
import math

from data.names.spells_to_print import spells_to_print

classes = {
      # 'class'    : ['icon'           , 'Color'],
        'barbarian': ['class-barbarian', 'BurlyWood'],
        'bard'     : ['class-bard'     , 'DarkOrange'],     # Done, but ehh
        'cleric'   : ['class-cleric'   , 'LightSteelBlue'], # Done, but maybe LightSlateGrey
        'druid'    : ['class-druid'    , 'YellowGreen'],
        'fighter'  : ['class-fighter'  , 'DimGray'],        # Done
        'monk'     : ['class-monk'     , 'Sienna'],         # Done
        'paladin'  : ['class-paladin'  , 'MediumPurple'],
        'ranger'   : ['class-ranger'   , 'DarkOliveGreen'], # Done
        'rogue'    : ['class-rogue'    , 'Black'],          # Done
        'sorcerer' : ['class-sorcerer' , 'BlanchedAlmond'],
        'warlock'  : ['class-warlock'  , 'PapayaWhip'],
        'wizard'   : ['class-wizard'   , 'Maroon'],         # Done
}

# Desired classes and the max spell level to print.
desired_classes = {
        'bard'   : 2,#6,
        'cleric' : 2,#6,
      # 'fighter': 0,
      # 'monk'   : 0,
        'ranger' : 1,#3,
      # 'rogue'  : 0,
        'wizard' : 2,#6,
}

# Extract the class tag(s).
def get_class_tags(spell_tags):
    class_tags = []
    for class_name in classes:
        if class_name in spell_tags:
            class_tags.append(class_name)

    return class_tags

# Extract the spell's level.
def get_spell_level(spell_tags):
    spell_level = spell_tags[1][0]
    if spell_level.isdigit():
        return int(spell_level)
    return 0

# Duplicates spells per desired class.
def filter_and_duplicate_per_class(spell_json, desired_spells = None):
    # Get some info from the spell.
    class_tags = get_class_tags(spell_json['tags'])
    spell_level = get_spell_level(spell_json['tags'])
    spell_name = spell_json['title']

    # Create as many copies of this spell as there are desired classes.
    spells = []
    for class_name in class_tags:
        # If we don't want to print this spell.
        if class_name not in desired_classes or \
            spell_level > desired_classes[class_name] or \
            spell_name not in desired_spells['spells']:
            continue

        # Making sure this class wants this spell.
        indices = [i for i, x in enumerate(desired_spells['spells']) if x == spell_name]
        classes_that_want_this_spell = [desired_spells['classes'][i] for i in indices]
        if class_name not in classes_that_want_this_spell:
            continue

        # Copy spell and change the color and icon_back.
        copy_spell = copy.deepcopy(spell_json)
        copy_spell['color'] = classes[class_name][1]
        copy_spell['icon_back'] = classes[class_name][0]
        copy_spell['tags'].append(class_name + '-copy')
        spells.append(copy_spell)

    return spells

# Helps to sort spells by class, then by PHB page.
def sorting_key(spell_json):
    class_tag = spell_json['tags'][-1]
    phb_page = spell_json['contents'][-1] \
                        .split(' ')[-1] \
                        .split('-')[0]

    return (class_tag, phb_page)

if __name__ == "__main__":
    combined_spells = []
    if len(sys.argv) != 2:
        print("Usage: python separate-into-classes.py true/false")
        print("Where 'true/false' is whether the script should use")
        print("data/names/{bard,cleric,wizard,ranger}.txt to filter spells")
        sys.exit(1)
    filter_spells = sys.argv[1].lower()
    if filter_spells == "true":
        spells_to_print.get_spells_to_print()
        wanted_spell_names = spells_to_print.spells_to_print
        print(wanted_spell_names)

    # Grab each spell and duplicate if needed.
    with open('data/cleaned/all-spells.json', 'r') as f:
        spell_data = json.load(f)
        for spell in spell_data:
            if filter_spells == "true":
                combined_spells += filter_and_duplicate_per_class(spell, wanted_spell_names)
            else:
                combined_spells += filter_and_duplicate_per_class(spell)

    # Sort spells by class, then by PHB page.
    combined_spells = sorted(combined_spells, key=sorting_key)

    # Count how many spells there are for each class.
    class_count = {"bard-copy": 0, "cleric-copy": 0, "ranger-copy": 0, "wizard-copy": 0}
    for spell in combined_spells:
        class_count[spell['tags'][-1]] += 1
    print(class_count)
    print("Total:", len(combined_spells))

    # Calculate total cost of printing all spells.
    pages = math.ceil(len(combined_spells) / 9.0)
    pages_cost = pages * 15
    sides_cost = (pages * 2) * 88
    print("Total Cost: $%.2f" % ((pages_cost + sides_cost) / 100))


    # Save all spells together.
    with open('data/cleaned/print-ready-spells.json', 'w') as f:
        f.write(json.dumps(combined_spells, indent=2, sort_keys=True))
