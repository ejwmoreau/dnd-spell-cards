# DnD Spell Card Generator

This generator helps create decent-looking Spell Cards in print-ready format.

The spell data needs to be organised in a particular format, which some of the
scripts in here achieve (depending on the previous spell data format).

The generator is originally from here:

- https://github.com/crobi/rpg-cards

## Adding Spells to be Printed

To add spells to be printed, please navigate to `data/names/` and input all the
desired spells in your class file there :)
