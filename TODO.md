# Spells to Print

* Cleric (Lvl 1 - 6)
* Bard (Lvl 1 - 6)
* Wizard (Lvl 1 - 6)
* Ranger (Lvl 1 - 3)
* Monk (Kristy chooses)
* Fighter (Ned chooses)
* Rogue (Scott chooses)

# Use the rpg-cards generator

* Customise settings based on what players want for their spell cards
* Need to create the pdfs out of html properly (Chromium on Arch seems to be ehh)

# Change the Spell Data

## Sources

* ";" separated spell data from hardcodex.ru (Downloaded)
    * Not sure if descriptions have been shrunk (probably raw)
    * Contains all PHB spells (and their official names)
* JSON SRD spell data from reddit.com/r/dndnext/comments/51hm37
    * Descriptions have been shrunk to fit on spell cards
    * Missing some spells and some are renamed (see reddit post)

## Changes left to be made

* Allow any icons from game-icons.net to be used (instead of only local ones)
