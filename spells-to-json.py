import json

# Page Numbers in PHB for all missing spells.
page_nums = {
    "Arcane Gate": "214-5",
    "Armor of Agathys": "215",
    "Arms of Hadar": "215",
    "Aura of Life": "216",
    "Aura of Purity": "216",
    "Aura of Vitality": "216",
    "Banishing Smite": "216",
    "Beast Sense": "217-8",
    "Blade Ward": "218-9",
    "Blinding Smite": "219",
    "Chromatic Orb": "221",
    "Circle of Power": "221-2",
    "Cloud of Daggers": "222",
    "Compelled Duel": "224",
    "Conjure Barrage": "225",
    "Conjure Volley": "226",
    "Cordon of Arrows": "228",
    "Crown of Madness": "229-30",
    "Crusader's Mantle": "230",
    "Destructive Wave": "231",
    "Dissonant Whispers": "234",
    "Elemental Weapon": "237",
    "Ensnaring Strike": "237",
    "Feign Death": "240",
    "Friends": "244",
    "Grasping Vine": "246",
    "Hail of Thorns": "249",
    "Hex": "251",
    "Hunger of Hadar": "251",
    "Lightning Arrow": "255",
    "Phantasmal Force": "264",
    "Power Word Heal": "266",
    "Ray of Sickness": "271",
    "Searing Smite": "274",
    "Staggering Smite": "278",
    "Swift Quiver": "279-80",
    "Telepathy": "281",
    "Thorn Whip": "282",
    "Thunderous Smite": "282",
    "Tsunami": "284",
    "Witch Bolt": "289",
    "Wrathful Smite": "289"
}

def add_spell(new_spell, converted_spells):
    # Check if spell is already in the converted spell list.
    # If yes, then just add a tag for a new class for that spell (in correct position).
    # If no, then append this new spell.
    for added_spell in converted_spells:
        if added_spell['title'] == new_spell['title']:
            comp = ['verbal', 'somatic', 'material']
            for c in comp:
                if c in added_spell['tags']:
                    index = added_spell['tags'].index(c)
                    added_spell['tags'].insert(index, new_spell['tags'][3])
                    break
            break
    else:
        converted_spells.append(new_spell)
    return converted_spells


def split_descr(descr):
    materials = ''
    higher_lvls = ''

    # Materials.
    materials = ''
    if descr[0] == '(':
        materials = ' ' + descr.split(')', 1)[0] + ')'
        descr = descr.split(')', 1)[1]

    # Higher Levels.
    if 'At Higher Levels:' in descr:
        descr, higher_lvls = descr.split('At Higher Levels: ')

    return [materials, descr, higher_lvls]


def spell_to_json(spell_line):
    _, name, lvl_type, time, s_range, comp, s_duration, descr, s_class = spell_line.strip(";").split(";")
    materials, descr, higher_lvls = split_descr(descr)
    s_class = s_class.lower().strip().split(' ')[0]
    ritual = ' (ritual)' if '(ritual)' in name else ''
    name = name.rsplit(' ', 1)[0] if '(ritual)' in name else name

    spell_json = {"color": "",
                  "contents": [],
                  "count": 1,
                  "icon": "TODO",
                  "icon_back": "robe",
                  "tags": [],
                  "title": ""}

    # Spell Name.
    spell_json['title'] = name

    # Spell Level and Type.
    spell_lvl_type = 'subtitle | ' + lvl_type + ritual
    spell_json['contents'].append(spell_lvl_type)

    # Rule.
    spell_json['contents'].append('rule')

    # Casting Time.
    casting = 'property | Casting time | ' + time
    spell_json['contents'].append(casting)

    # Range.
    rang = 'property | Range | ' + s_range
    spell_json['contents'].append(rang)

    # Components.
    components = 'property | Components | ' + "".join(comp.split(", ")) + materials
    spell_json['contents'].append(components)

    # Duration.
    duration = 'property | Duration | ' + s_duration
    spell_json['contents'].append(duration)

    # Rule and Fill.
    spell_json['contents'].append('rule')
    spell_json['contents'].append('fill | 2')

    # Description.
    text = 'text | ' + descr
    spell_json['contents'].append(text)

    # Fill.
    spell_json['contents'].append('fill | 3')

    # At Higher Levels.
    if higher_lvls != '':
        section = 'section | At higher levels'
        higher_lvls = 'text | ' + higher_lvls
        spell_json['contents'] += [section, higher_lvls]

    # Source.
    spell_json['contents'].append('source | PHB ' + page_nums[name])

    # Tags.
    tag_lvl = 'cantrip' if 'cantrip' in lvl_type else lvl_type.rsplit(" ", 1)[0]
    tag_type = lvl_type.split(" ")[0] if 'cantrip' in lvl_type else lvl_type.rsplit(" ", 1)[1]
    comp_map = {'V': 'verbal', 'S': 'somatic', 'M': 'material'}
    tag_comp = []
    for c in comp.split(", "):
        tag_comp.append(comp_map[c])
    spell_json['tags'] = ['PHB', tag_lvl, tag_type.lower(), s_class] + tag_comp

    return spell_json


if __name__ == "__main__":
    converted_spells = []
    with open('data/raw/missing-spells.csv', 'r') as f:
        for spell_line in f:
            converted_spell = spell_to_json(spell_line.strip())
            converted_spells = add_spell(converted_spell, converted_spells)

    with open('data/raw/missing-spells.json', 'w') as f:
        f.write(json.dumps(converted_spells, indent=2, sort_keys=True))
