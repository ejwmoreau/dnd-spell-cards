# Files

## missing-spells.json
* Contains the json formatted spell data for all missing spells.
* Descriptions have not been shortened.

## spell-data-srd.json
* Contains the json formatted spell data for all Open Source spells.
* Descriptions have already been shortened.

# Shortening Descriptions
* Some of the missing spells already have a short enough description.
* You can check if it fits, by using the rpg-cards folder given.
* Open the rpg-cards/generator/generator.html page and click "Load File"
  to load the json spell data. Then, using the little side preview, you can
  see if the description will fit in the card.
* You can also click "Generate", which will open a html page with all the
  spells loaded at the same time.
