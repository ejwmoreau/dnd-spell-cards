# Example File

```
# You can use '#' for comments.
# Blank lines will be ignored.

# Put each spell name on a separate line.
Fire Bolt
Dissonant Whispers
Sacred Flame
Melf's Acid Arrow

```

Spell names should follow the official name in the Player's Handbook. See
`all-spell-names.txt` for the names of all spells.

WARNING: Once each of the specified spells are printed, the printed spell names
will be moved to `printed.txt`. As a result, only the comments and blank lines
will be left in the class files.
