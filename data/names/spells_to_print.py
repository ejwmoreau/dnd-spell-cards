import sys

class SpellsToPrint():
    def __init__(self):
        self.classes = ['bard', 'cleric', 'ranger', 'wizard']
        self.spells_to_print = {'spells': [], 'classes': []}

        self.spells_printed = {}
        for c in self.classes:
            self.spells_printed[c] = []

        self.got_spells_to_print = False

    def readLine(self, line, valid=True):
        line = line.strip()
        if len(line) == 0 or line.startswith('#'):
            return [] if valid else [line]
        return [line] if valid else []

    def get_spells_to_print(self):
        for dnd_class in self.classes:
            with open('data/names/' + dnd_class + '.txt', 'r') as f:
                for line in f:
                    self.spells_to_print['spells'] += self.readLine(line)
                    self.spells_to_print['classes'].append(dnd_class)
        self.got_spells_to_print = True

    def set_spells_aside_for_printing(self):
        if not self.got_spells_to_print:
            print("WARNING: Trying to set aside spells that haven't been loaded.")
            sys.exit(1)

        # Saving all current spells for printing to a file.
        with open('spells-for-printing.txt', 'w') as f:
            for dnd_class in self.classes:
                f.write('# {}\n'.format(dnd_class.title()))
                for spell in self.spells_to_print[dnd_class]:
                    f.write('{}\n'.format(spell))
                f.write('\n')

        # Wiping the spell files.
        for dnd_class in self.classes:
            saved_lines = []
            with open(dnd_class + '.txt', 'r') as f:
                for line in f:
                    saved_lines += self.readLine(line, False)
            with open(dnd_class + '.txt', 'w') as f:
                for line in saved_lines:
                    f.write('{}\n'.format(line))

    def get_printed_spells():
        with open('printed.txt', 'r') as f:
            for line in f:
                self.spells_printed[dnd_class] += self.readLine(line)

spells_to_print = SpellsToPrint()
